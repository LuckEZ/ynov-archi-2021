import express from "express";
import dotenv from "dotenv";
dotenv.config();

const app = express();
const PORT = process.env.PORT || 8000;

app.get("/", (req, res) => {
  res.status(200).json({ message: "Hello World!" });
});

app.listen(PORT, () => {
  // tslint:disable-next-line:no-console
  console.log(`App is listening at http://localhost:${PORT}`);
});

export default app;
